# coding: utf-8

from odoo import api, models, _
from odoo.fields import *

import logging
log = logging.getLogger().info


class ResPartner(models.Model):
    _inherit = 'res.partner'

    birth = Date(_('Birth'))
    vehicle = Char(_('Vehicle'))
