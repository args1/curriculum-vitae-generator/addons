# coding: utf-8

from odoo import api, models, _
from odoo.fields import *

import logging
log = logging.getLogger().info

import random


class Cv(models.Model):
    _name = 'cv'
    _description = _('CV')
    _order = 'write_date desc'

    @api.depends('name', 'partner_id', 'report_id',
        'graduation_ids',
            'graduation_ids.name',
            'graduation_ids.sequence',
        'hobby_ids', 'experience_ids', 'skill_ids')
    def _cpt_pdf(self):
        for cv_id in self:
            cv_id.pdf = ''
            current_id = cv_id.id
            if type(current_id) is not int:
                try:
                    current_id = int(str(current_id).split('_')[-1])
                except:
                    pass
            if cv_id.report_id and type(current_id) is int:
                pdf_url = '/report/pdf/%s/%s' % (cv_id.report_id.report_name, current_id)
                pdf = '''<div>
                    <iframe width="100%%" height="600rem" src="%s"/>
                  </div>
                ''' % (pdf_url)
                cv_id.pdf = pdf

    @api.model
    def default_get(self, fields):
        res = super().default_get(fields)
        res['partner_id'] = self.env.user.partner_id.id
        report_ids = self.env['ir.actions.report'].search([
            ('model', '=', self._name),
        ])
        res['report_id'] = random.choice(report_ids.ids)
        return res

    active = Boolean(_('Active'), default=True)
    name = Char(_('Name'))
    pdf = Text(_('PDF'), compute='_cpt_pdf')

    partner_id = Many2one('res.partner', _('Identity'), required=True)
    report_id = Many2one('ir.actions.report', _('Template'), domain=[
        ('model', '=', _name),
    ])

    graduation_ids = Many2many('cv.line.advanced', relation='rel_cv_graduation', string=_('Diplomas and training'))
    hobby_ids = Many2many('cv.line', relation='rel_cv_hobby', string=_('Hobby'))
    experience_ids = Many2many('cv.line.advanced', relation='rel_cv_experiences', string=_('Professional experiences'))
    skill_ids = Many2many('cv.line', relation='rel_cv_skills', string=_('Professional skills'))
