# coding: utf-8

from odoo import api, models, _
from odoo.fields import *


class CvLine(models.Model):
    _name = 'cv.line'
    _description = _('CV line')
    _order = 'sequence'

    name = Char(_('Name'), required=True)
    sequence = Integer(_('Sequence'), help=_('Used to sort lines.'))
