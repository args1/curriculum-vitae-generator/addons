# coding: utf-8

from odoo import api, models, _
from odoo.fields import *

import locale

import logging
log = logging.getLogger().info


class CvLineAdvanced(models.Model):
    _name = 'cv.line.advanced'
    _description = _('CV advanced line')
    _inherit = 'cv.line'
    _order = 'sequence, date_end desc, date_start'

    date_end = Date(_('Date end'))
    date_format = Char(_('Date preview'), compute='_cpt_date_format')
    date_preview = Char(_('Date format'), compute='_cpt_date_preview')
    date_start = Date(_('Date start'), required=True)
    description = Html(_('Description'))
    date_separator = Selection([
        ('/', '/'),
        ('-', '-'),
        (' ', _('Space')),
    ], string=_('Date separator'), required=True, default='/')
    show_day_of_month = Boolean(_('Show day of month'), default=True)
    month_format = Selection([
        ('%_b', _('Shorten name')),
        ('%B', _('Full month name')),
        ('%m', _('Month number')),
        ('hide', _('Hidden')),
    ], string=_('Month format'), default='%m', required=True)
    year_format = Selection([
        ('%Y', _('Full')),
        ('%_y', _('Hide century')),
        ('hide', _('Hidden')),
    ], string=_('Year format'), default='%Y', required=True)

    @api.depends('date_end', 'date_separator', 'date_start', 'month_format',
        'show_day_of_month', 'year_format')
    def _cpt_date_format(self):
        for line_id in self:
            line_id.date_format = ''
            date_format = ''
            if line_id.show_day_of_month:
                date_format += '%d'
            if line_id.month_format != 'hide':
                if date_format:
                     date_format += line_id.date_separator
                date_format += line_id.month_format.replace('_', '')
            if line_id.year_format != 'hide':
                if date_format:
                     date_format += line_id.date_separator
                date_format += line_id.year_format.replace('_', '')
            line_id.date_format = date_format

    @api.depends('date_format')
    def _cpt_date_preview(self):
        for line_id in self:
            log(self.env.context)
            locale.setlocale(locale.LC_ALL, self.env.context['lang'] + '.UTF-8')
            line_id.date_preview = ''
            if not line_id.date_format or not line_id.date_start:
                continue
            line_id.date_preview =  line_id.date_start.strftime(line_id.date_format)
            if line_id.date_end:
                line_id.date_preview += _(' to ') + line_id.date_end.strftime(line_id.date_format)
