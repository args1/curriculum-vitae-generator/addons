# coding: utf-8

{
    'name': 'CV Generator',

    'summary': 'CV Generator',

    'description': 'CV Generator',

    'author': 'Sirfanas',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'web_widget_colorpicker',
    ],

    'data': [
        'security/ir.model.access.csv',
        'datas/ir_rule.xml',

        'views/cv_view.xml',
        'views/cv_line_advanced_view.xml',
        'views/res_partner_view.xml',
    ],

    'application': True,
}
