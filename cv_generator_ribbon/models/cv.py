# coding: utf-8

from odoo import api, models, _
from odoo.fields import *

import logging
log = logging.getLogger().info


class Cv(models.Model):
    _inherit = 'cv'
    _sql_constraints = [
        ('margin_bottom_positive', 'CHECK(margin_bottom >= 0)', _('Margin bottom must be positive !'))
    ]

    ribbon_left_panel_background = Char(_('Left panel background'), default='rgba(0,79,255,1)')
    ribbon_left_panel_color = Char(_('Left panel color'), default='rgba(255,255,255,1)')
    ribbon_margin_bottom = Integer(_('Margin bottom'))
    ribbon_right_panel_background = Char(_('Right panel background'), default='rgba(255,255,255,1)')
    ribbon_right_panel_color = Char(_('Right panel color'), default='rgba(0,0,0,1)')
    ribbon_subtitle_color = Char(_('Subtitle color'), default='rgba(75,75,75,1)')
    ribbon_timeline_circle_color = Char(_('Timeline circle color'), default='rgba(0,79,255,1)')
    ribbon_timeline_line_color = Char(_('Timeline line color'), default='rgba(82,137,255,1)')
    ribbon_timeline_title_color = Char(_('Timeline title color'), default='rgba(75,75,75,1)')
    ribbon_title_color = Char(_('Title color'), default='rgba(0,0,0,1)')
