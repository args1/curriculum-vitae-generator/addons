# coding: utf-8

{
    'name': 'CV Generator - Ribbon',

    'summary': 'CV Generator - Ribbon',

    'description': 'CV Generator - Ribbon',

    'author': 'Sirfanas',

    # any module necessary for this one to work correctly
    'depends': [
        'cv_generator',
    ],

    'data': [
        'views/cv_view.xml',

        'report/cv_ribbon.xml',
    ],

    'application': True,
}
